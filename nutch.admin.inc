<?php

/**
 * @file
 * Implements admin functionality relating to Nutch setup and running
 */


/**
 * Settings form for Nutch setup
 * @TODO Need to add all of the extra crawl options 
 *        - topN: -topN, then instead of all unfetched urls you only get N urls with the highest score
 *        - threads
 *        - depth
 *        - adddays
 */
function nutch_admin_settings() {
  nutch_check_directory('logs');
  
  $form = array();
  $form['system'] = array(
    '#type' => 'fieldset',
    '#title' => t('System settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['system']['nutch_nutch_dir'] = array(
    '#type' => 'textfield',
    '#title' => t('NUTCH_HOME'),
    '#description' => t('The absolute path to where the Nutch distribution is found, so that $NUTCH_HOME/bin/nutch resolves to the main Nutch script.'),
    '#default_value' => variable_get('nutch_nutch_dir', '/usr/local/nutch'),
    '#required' => TRUE,
  );
  $form['system']['nutch_java'] = array(
    '#type' => 'textfield',
    '#title' => t('JAVA_HOME'),
    '#description' => t('The absolute path to a directory which is JAVA_HOME. $JAVA_HOME/bin/java should resolve to the Java binary that will run Nutch.'),
    '#default_value' => variable_get('nutch_java', '/usr'),
    '#required' => TRUE,
  );

  if (!$nutch_solr  = variable_get('nutch_solr', FALSE)) {
    $host = variable_get('apachesolr_host', 'localhost');
    $port = variable_get('apachesolr_port', '8983');
    $path = variable_get('apachesolr_path', '/solr');
    $nutch_solr = sprintf('http://%s:%s%s', $host, $port, $path);
  }
  $form['system']['nutch_solr'] = array(
    '#type' => 'textfield',
    '#title' => t('SOLR URL'),
    '#description' => t('The url of your apache solr instance.'),
    '#default_value' => $nutch_solr,
    '#required' => TRUE,
  );
  $form['system']['nutch_crawl_depth'] = array(
    '#type' => 'select',
    '#title' => t('Per host URL fetch limit'),
    '#options' => drupal_map_assoc(array(1, 2, 5, 10)),
    '#default_value' => variable_get('nutch_crawl_depth', 2),
    '#description' => t('The depth to follow links on a page'),
  );
  $form['system']['nutch_topN'] = array(
    '#type' => 'select',
    '#title' => t('Top URLs by Score'),
    '#options' => drupal_map_assoc(array(1, 5, 10, 20, 50, 100)),
    '#default_value' => variable_get('nutch_topN', 100),
    '#description' => t('Grab the top N URLs based on nutch Score.'),
  );   
  $form['system']['nutch_urls_to_fetch'] = array(
    '#type' => 'select',
    '#title' => t('The number of URLs to fetch and index'),
    '#options' => drupal_map_assoc(array(10, 20, 50, 100, 200, 500, 750, 1000, 2000, 5000, 10000)),
    '#default_value' => variable_get('nutch_urls_to_fetch', 100),
    '#description' => t('When crawl is run, this number of URLs will be fetched and indexed. It is important to know how long this takes on your equipment so as to avoid starting overlapping jobs. Always start with small numbers (hundreds) and profile how long it takes before moving up.'),
  ); 
  $form['system']['nutch_commit'] = array(
    '#type' => 'checkbox',
    '#title' => t('Send to Solr'),
    '#description' => t('Send to Solr at the end of the Nutch crawl'),
    '#default_value' => variable_get('nutch_commit', 1),
  );
  $form['system']['nutch_crawl_on_cron'] = array(
    '#type' => 'checkbox',
    '#title' => t('Run crawl on cron'),
    '#default_value' => variable_get('nutch_crawl_on_cron', 0),
  );
  
  return system_settings_form($form);
}

function nutch_admin_crawl() {
  nutch_check_directory('logs', TRUE);
  
  $form = array();
  $form['controls'] = array(
    '#type' => 'fieldset',
    '#title' => t('crawl controls'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );
  $form['controls']['nutch_commit'] = array(
    '#type' => 'checkbox',
    '#title' => t('Send to Solr'),
    '#description' => t('Send to Solr at the end of the Nutch crawl'),
    '#default_value' => variable_get('nutch_commit', 1),
  );    
  $form['controls']['nutch_debug'] = array(
    '#type' => 'checkbox',
    '#title' => t('Dry run/debug crawl'),
    '#default_value' => variable_get('nutch_debug', 0),
  );  
  $form['controls']['crawl'] = array(
    '#type' => 'submit',
    '#value' => t('Start Crawl'),
  );
  return $form;
}

function nutch_admin_crawl_submit($form, &$form_state) {
  $nutch_commit = (int) $form_state['values']['nutch_commit'];
  $nutch_debug = (int) $form_state['values']['nutch_debug'];
  variable_set('nutch_commit', $nutch_commit);
  variable_set('nutch_debug', $nutch_debug);
  nutch_start_crawl(
           variable_get('nutch_nutch_dir', '/usr/local/nutch'), 
           variable_get('nutch_java', '/usr'), 
           variable_get('nutch_solr', FALSE), 
           variable_get('nutch_seed_url', 'http://localhost'), 
           $nutch_commit, 
           $nutch_debug,
           array(
             'nutch_urls_to_fetch'      => variable_get('nutch_urls_to_fetch', 100), 
             'nutch_url_filters'        => variable_get('nutch_url_filters', NULL), 
             'nutch_mimetype_blacklist' => variable_get('nutch_mimetype_blacklist', NULL), 
             'nutch_protocol_blacklist' => variable_get('nutch_protocol_blacklist', NULL)
           )
  );
}
/**
 * 
 * Runbot controller
 * @param string $nutch_home
 * @param string $java_home
 * @param stroing $solr_url
 * @param stroing $seed_urls 
 * @param boolean $commit
 * @param boolean $debug
 * @param array   $options
 */
function nutch_start_crawl($nutch_home, $java_home, $solr_url, $seed_urls='', $commit=0, $debug=0, $options=array()) {
  $args = func_get_args();
  // Path to Runbot controller script
  $command =  $_SERVER['DOCUMENT_ROOT'] . base_path() . drupal_get_path('module', 'nutch') . '/' .'runbot';

  if (empty($nutch_home) || empty($java_home) || empty($solr_url)) {
    drupal_set_message(t("You must supply a nutch home directory, JAVA_HOME and Solr URL; crawl aborted."));
    return 0;
  }  
  
  $command .= ' -n ' . escapeshellarg($nutch_home);
  $command .= ' -j ' . escapeshellarg($java_home);
  $command .= ' -s ' . escapeshellarg($solr_url);

  if (!empty($seed_urls)) {
    /* 
     * Replace all of the line feeds with ! because the
     * shell scipt doesnt recognise them when adding them
     * to the urls file. We could run echo -e but its not
     * a universal command 
     */
    $seed_urls_replaced = str_replace("\n", '!', $seed_urls);
    if ($debug == 1) drupal_set_message($seed_urls_replaced);
    $command .= ' -u ' . escapeshellarg($seed_urls_replaced);
  }

  $command .= ' -c ' . escapeshellarg($commit);
  $command .= ' -d ' . escapeshellarg($debug);
  
  foreach ($options as $opt_key => $opt_val) {
    switch ($opt_key) {
      case 'nutch_urls_to_fetch':
          $command .= ' -f ' . escapeshellarg($opt_val);
        break;
      case 'nutch_url_filters':
          $opt_val = str_replace("\n", '\!', $opt_val);
          $opt_val = addcslashes($opt_val, '/');
          $command .= ' -i ' . escapeshellarg($opt_val);
        break;
      case 'nutch_mimetype_blacklist':
        $command .= ' -b ' . escapeshellarg($opt_val);
        break;
      case 'nutch_protocol_blacklist':
        $command .= ' -p ' . escapeshellarg($opt_val);
        break;
    }
  }
  
  drupal_set_message(t("Starting Nutch Crawl."));
  if ($debug == 1) {
    drupal_set_message($command);
    $rtn = exec($command . ' &', $output);
    $rtn_output = $debug;
    foreach ($output as $line) {
      $rtn_output .= $line . "<br/>";
    }
    drupal_set_message($rtn_output);     
  }
  else{
    $tmp = array();
    $process = proc_open($command . ' &', array(), $tmp);
    drupal_set_message('Command sent, please check the ' . l('Nutch Crawl Logs', '/admin/settings/nutch/logs') . ' for progress/errors');
    $rtn = proc_close($process);
  }
}

function nutch_admin_seed() {
  $form = array();
  $form['controls'] = array(
    '#type' => 'fieldset',
    '#title' => t('seed controls'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  /* @TODO DO a compare of the current seed files on disk and that saved in the variables table
             if different set a message */ 
  $form['controls']['nutch_seed_url'] = array(
    '#type' => 'textarea',
    '#title' => t('Seed URLs'),
    '#description' => t('The URL/s Nutch uses seed crawl. Place URLs on a separate line. They must be prefixed with http://'),
    '#default_value' => variable_get('nutch_seed_url', 'http://localhost'),
    '#required' => TRUE,
  );
  $form['controls']['nutch_url_filters'] = array(
    '#type' => 'textarea',
    '#title' => t('Filter URLs'),
    '#description' => t('Filter Regex for defining your crawl criteria. Please urls on a separate line'),
    '#default_value' => variable_get('nutch_url_filters', "+^http://localhost\n-."),
    '#required' => TRUE,
  );  
  $form['controls']['nutch_mimetype_blacklist'] = array(
    '#type' => 'textarea',
    '#title' => t('Mimetype Blacklist'),
    '#description' => t('List of mime types to ignore'),
    '#default_value' => variable_get('nutch_mimetype_blacklist', "swf|SWF|doc|DOC|mp3|MP3|WMV|wmv|txt|TXT|rtf|RTF|avi|AVI|m3u|M3U|flv|FLV|WAV|wav|mp4|MP4|avi|AVI|rss|RSS|xml|XML|pdf|PDF|js|JS|gif|GIF|jpg|JPG|png|PNG|ico|ICO|css|sit|eps|wmf|zip|ppt|mpg|xls|gz|rpm|tgz|mov|MOV|exe|jpeg|JPEG|bmp|BMP"),
    '#required' => TRUE,
  );   
  $form['controls']['nutch_protocol_blacklist'] = array(
    '#type' => 'textfield',
    '#title' => t('Protocol Blacklist'),
    '#description' => t('List of protocol links to ignore'),
    '#default_value' => variable_get('nutch_protocol_blacklist', "https|telnet|file|ftp|mailto"),
    '#required' => TRUE,
  );
  return system_settings_form($form);
}

function nutch_admin_inject_submit($form, &$form_state) {
  drupal_set_message(t("Nutch is injecting URLs."));
  nutch_do_inject(variable_get('nutch_nutch_dir', '/usr/local/nutch'), variable_get('nutch_crawl_dir', '/usr/local/nutch/crawl'), variable_get('nutch_urls_dir', '/usr/local/nutch/seed'));
}

function nutch_admin_logs() {
  $output = '';
  $log_directory = variable_get('nutch_nutch_dir', '/usr/local/nutch') . '/logs';
  $log_file = $log_directory . '/hadoop.log';
  nutch_check_directory('logs', TRUE);
  
  if (!is_readable($log_file)) {
    drupal_set_message(t('Cannot access  %log either it does not exist or is not writable by the webserver.', array('%log' => $log_file)), 'error');
  }
  else{
    $output = check_plain(file_get_contents($log_file));
  }

  $form = array(
    'log' => array(
      '#type' => 'textarea',
      '#title' => $log_file,
      '#value' => $output,
      '#rows' => 30,
    ),
  );
  return $form;
}

/**
 * 
 * Check whether the directory as the correct permissions
 * @param unknown_type $directory
 * @param unknown_type $show_form_errors
 */
function nutch_check_directory($directory, $show_form_errors = FALSE) {
  $directory = variable_get('nutch_nutch_dir', '/usr/local/nutch') . "/$directory";
  $success = FALSE;
  if ($show_form_errors) {
    $success = file_check_directory($directory, TRUE, 'nutch_check_directory');
  }
  else {
    $success = file_check_directory($directory, TRUE);
  }
  if (!$success) {
    $help = t("Make sure that %directory is executable and writable by the webserver user (usually <em>www-data</em> or <em>nobody</em>). The parent directories may need these permissions too.", array('%directory' => $directory));
    drupal_set_message($help, 'warning');
  }  
}

function nutch_get_current_seed_urls() {
  $seed_file = variable_get('nutch_nutch_dir', '/usr/local/nutch') .'/seed/urls';
  $output = check_plain(file_get_contents($seed_file));
  if (!$output) {
    $output = '';
  }
  return $output;
}
