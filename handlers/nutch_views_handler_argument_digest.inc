<?php

/**
 * @file
 * defines a class to handle digest arguments
 */
class nutch_views_handler_argument_digest extends apachesolr_views_handler_argument {
  function title() {
    $this->query->set_query("*:*");
    return $this->digest($this->argument);
  }

  function digest($digest) {
      return check_plain($digest);
  }
}