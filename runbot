#!/bin/bash

# Runs the Nutch bot to crawl or re-crawl
# Usage: bin/runbot [safe]
#        If executed in 'safe' mode, it doesn't delete the temporary
#        directories generated during crawl. This might be helpful for
#        analysis and recovery in case a crawl fails.
#
# if no args specified, show usage
if [ $# = 0 ]; then
  echo "Usage: runbot [-n nutch home] [-j java home] [ -s Solr url]  [-f number of URls to fetch] [-u Seed URLs ] [-d debug/dryrun]"
  echo "  -n    Required. The absolute path to the nutch home,"
  echo "        such that \$NUTCH_HOME/bin/nutch resolves"
  echo "        to the nutch script."
  echo "  -j    Required. Java Home value"
  echo "  -s    Required. complete URL to your Solr instance e.g. http://localhost:8983/solr."
  echo "        "
  echo "  -c    Optional. Commit, whether or not to commit to crawl to solr."
  echo "  -f    Optional. An integer which limits how many URLs will be fetched per run. "
  echo "                   The will default to 100"
  echo "  -u    Optional. List of URLs separated by ! used as the seeds of the crawl"
  echo "        "
  echo "  -d    Optional. Debug set to 1 will allow you to do a dry-run of the commands called "
  echo "        "
  echo "  -i    Optional. URL filters - Restrict the scope of the urls found to a specific domains or patterns "
  echo "        "  
  echo "  -b    Optional. Mime type blacklist - Skip over any mime type found in this list "
  echo "        "    
  echo "  -p    Optional. Protocol blacklist - Skip over any protocol found in this list "
  echo "        "
  echo "        NOTE: This script setup is expecting the location of your url seeds to be in a "
  echo "               directory called 'seed', the file should be call url"    
  exit 1
fi

# set flag vars to empty
n= j= s= f=100 d=0 c=0 u= i= b= p=

# leading colon so that we do error handling here
while getopts :n:j:f:s:d:c:u:i:b:p: opt
  do
  case $opt in
  n)  n=$OPTARG
      ;;
  j)  j=$OPTARG
      ;;
  f)  f=$OPTARG
      ;;
  s)  s=$OPTARG
      ;;
  d)  d=$OPTARG
      ;;
  c)  c=$OPTARG
      ;;
  u)  u=$OPTARG
      ;;
  i)  i=$OPTARG
      ;;
  b)  b=$OPTARG
      ;;
  p)  p=$OPTARG
      ;;
  '?')  echo "$0: invalid option -$OPTARG" >&2
        echo "USAGE: runbot [-n nutch home] [-j java home] [ -s Solr url]  [-f number of URls to fetch] [-u Seed URLs ] [-d debug/dryrun]"
  esac
done

if [ $d == 1 ]
then
  echo "Nutch Home: $n"
  echo "JAVA HOME: $j"
  echo "URLs to Crawl: $f"
  echo "Commit to Solr: $c"
  echo "Solr URL: $s"
  echo "Seed URL: $u"
  echo "Debug: $d"
  echo "Running as: `whoami`"
  echo "Filter URLs: $i"
  echo "Mime Type Blacklist: $b"
  echo "Protocol Blacklist: $p"
  
fi

  if [ -z "$n" ] || [ -z "$j" ] || [ -z "$s" ]; then
    echo " [-n nutch home] [-j java home] [ -s Solr url] are required"
    exit 1
  fi
export JAVA_HOME=$j
export NUTCH_HOME=$n

NAME="nutch"
LOCK="/tmp/${NAME}.lock"
if [ -f "$LOCK" ]
 then
  echo "Process already running..."
  exit 0
else
  echo "create lock"
  echo $$ >> "$LOCK"
fi

depth=2
threads=50
adddays=5
topN=2 # Comment this statement if you don't want to set topN value
regex_file=$NUTCH_HOME/conf/regex-urlfilter.txt

if [ -n "$u" ]
then
  if [ $d == 1 ]
  then
    echo "-- Seed Urls -- "
    echo "$u" | tr "!" "\n"
  else
    echo "$u" | tr "!" "\n" > $NUTCH_HOME/seed/urls
  fi
fi

if [ -n "$i" ]
then
  echo "$i" | tr "!" "\n"
#  cat $regex_file

fi

if [ -n "$b" ] 
then
  echo "Mime Type Blacklist Placeholder"
fi

if [ -n "$p" ]
then
  echo "Protocol Blacklist Placeholder"
fi

#rm $LOCK
#exit 0

if [ -z "$NUTCH_HOME" ]
then
  NUTCH_HOME=.
  echo runbot: $0 could not find environment variable NUTCH_HOME
  echo runbot: NUTCH_HOME=$NUTCH_HOME has been set by the script
else
  echo runbot: $0 found environment variable NUTCH_HOME=$NUTCH_HOME
fi

if [ -n "$topN" ]
then
  topN="--topN $rank"
else
  topN=""
fi

steps=5
echo "----- Inject (Step 1 of $steps) -----"
if [ $d == 1 ]
then
  echo $NUTCH_HOME/bin/nutch inject $NUTCH_HOME/crawl/crawldb $NUTCH_HOME/seed
else
  cd $NUTCH_HOME
  $NUTCH_HOME/bin/nutch inject $NUTCH_HOME/crawl/crawldb $NUTCH_HOME/seed
fi

echo "----- Generate, Fetch, Parse, Update (Step 2 of $steps) -----"
for((i=0; i < $depth; i++))
do
  echo "--- Beginning crawl at depth `expr $i + 1` of $depth ---"
  if [ $d == 1 ]
  then
    echo $NUTCH_HOME/bin/nutch generate $NUTCH_HOME/crawl/crawldb $NUTCH_HOME/crawl/segments $topN -adddays $adddays
  else
    $NUTCH_HOME/bin/nutch generate $NUTCH_HOME/crawl/crawldb $NUTCH_HOME/crawl/segments $topN -adddays $adddays
  fi
  
  if [ $? -ne 0 ]
  then
    echo "runbot: Stopping at depth $depth. No more URLs to fetch."
    break
  fi
  segment=`ls -d $NUTCH_HOME/crawl/segments/* | tail -1`
 
  if [ $d == 1 ]
  then
    echo   $NUTCH_HOME/bin/nutch fetch $segment -threads $threads
  else
    $NUTCH_HOME/bin/nutch fetch $segment -threads $threads
  fi
  
  if [ $? -ne 0 ]
  then
    echo "runbot: fetch $segment at depth $depth failed. Deleting it."
    rm -rf $segment
    continue
  fi

  if [ $d == 1 ]
  then
   echo $NUTCH_HOME/bin/nutch updatedb $NUTCH_HOME/crawl/crawldb $segment
  else
   $NUTCH_HOME/bin/nutch updatedb $NUTCH_HOME/crawl/crawldb $segment
  fi
  
done

echo "----- Merge Segments (Step 3 of $steps) -----"
if [ $d == 1 ]
then
 echo $NUTCH_HOME/bin/nutch mergesegs $NUTCH_HOME/crawl/MERGEDsegments $NUTCH_HOME/crawl/segments/*
else
 $NUTCH_HOME/bin/nutch mergesegs $NUTCH_HOME/crawl/MERGEDsegments $NUTCH_HOME/crawl/segments/*
fi

if [ "$safe" != "yes" ]
then
  rm -rf $NUTCH_HOME/crawl/segments/*
else
  mkdir $NUTCH_HOME/crawl/FETCHEDsegments
  mv -v $NUTCH_HOME/crawl/segments/* $NUTCH_HOME/crawl/FETCHEDsegments
fi

if [ $d == 1 ]
then
  echo mv -v $NUTCH_HOME/crawl/MERGEDsegments/* $NUTCH_HOME/crawl/segments
  echo rmdir $NUTCH_HOME/crawl/MERGEDsegments
else
  mv -v $NUTCH_HOME/crawl/MERGEDsegments/* $NUTCH_HOME/crawl/segments
  rmdir $NUTCH_HOME/crawl/MERGEDsegments
fi

echo "----- Invert Links (Step 4 of $steps) -----"
if [ $d == 1 ]
then
 echo $NUTCH_HOME/bin/nutch invertlinks $NUTCH_HOME/crawl/linkdb $NUTCH_HOME/crawl/segments/*
else
 $NUTCH_HOME/bin/nutch invertlinks $NUTCH_HOME/crawl/linkdb $NUTCH_HOME/crawl/segments/*
fi

echo "----- Index and send to solr (Step 5 of $steps) -----"
if [ $d == 1 ]
then
 echo $NUTCH_HOME/bin/nutch solrindex $s $NUTCH_HOME/crawl/crawldb $NUTCH_HOME/crawl/linkdb $NUTCH_HOME/crawl/segments/* 
else
 if [ $c == 1 ]
 then
  $NUTCH_HOME/bin/nutch solrindex $s $NUTCH_HOME/crawl/crawldb $NUTCH_HOME/crawl/linkdb $NUTCH_HOME/crawl/segments/*
 fi
fi
echo "runbot: FINISHED: Crawl completed!"
rm $LOCK	
